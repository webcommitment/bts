<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'local' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'AK1sr/GLE6ejePH+JOrxDpqtTD8odiJfKXxfUeTKJakfUrLpvuwHnXxeItv1f5NbR1EarYIR5rK34HzCr8i0TQ==');
define('SECURE_AUTH_KEY',  '7WYyUBn+ErCVGWdQsENLifW9OWrinhvbkn4qi4/IbwRVGh5hHgtETH2jgefsEP2nwWjyr+nJ/BOm6JbjAHW3Cg==');
define('LOGGED_IN_KEY',    'PTtwUwMNGVK2AoNZ0UgNMcmat7tbzBKE5uaqO0XPvYeFaM/4J47AK3scr+JbDE3IBsRJ7KbhPk4IDD5NSKQotw==');
define('NONCE_KEY',        'IwtagWODEPestTPSuhWuq2R+0hn09uA377v9ODP8DpjmSWah3oyc5lyI01hLNKxdxUSvGimK2BtAjodNgSU3eQ==');
define('AUTH_SALT',        'oN1S2mOgxldi8F827TJGpwpo0bgVSlU6n+LKHqNFI5/vYyAD372fZZoi67FyI0f5uGm1n/456SDn8PXruWCsXw==');
define('SECURE_AUTH_SALT', 'cVxByw+4gPSbRcPgqF2vIfRxl4c6KBDM1Cp9/ssAi1A0x1UBlly+4Fjv1JGEFiz5nkqApVsuIjLtRwUBpqy4tQ==');
define('LOGGED_IN_SALT',   'J1qYlMdG+BAej+9nLSwqVvLgXd5xUa4/+jHeNLQMHvexY08NKVd5XlM54+Ys+xBskZ0tnqZGmx5yW7F94qnJmg==');
define('NONCE_SALT',       'unSu5JC0M0Ul3RSXSWA2mv6FHUEQPjNM3l9agWt57v8UzimI7/e3TKGskeEhORntGPVCyC0XidN0BC7RPSutQA==');

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';




/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
